
const baseUrlUsers = "http://localhost:8080/api/v1/users";
const baseUrlTasks = "http://localhost:8080/api/v1/tasks";
const baseUrlAuth = "http://localhost:8080/api/v1/auth";

export const userAPI = {
  fetchAllUsers: async (accessToken) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    const usersResponse = await fetch(baseUrlUsers + "/fetch-users", {
      method: "GET",
      headers: headers,
    });
    
    return usersResponse;
  },
  
  fetchUserByToken: async (accessToken) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    const response = await fetch(baseUrlUsers + "/fetch-user", {
      method: "POST",
      headers: headers,
      body: JSON.stringify({ token: accessToken }),
    });
    
    return await response.json();
  },
}; // fine



export const taskAPI = {
  fetchAllTasks: async (accessToken) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    const response = await fetch(baseUrlTasks + "/fetch-tasks", {
      method: "GET",
      headers: headers,
    });
    const tasksData = await response.json();
    return tasksData;
  },

  fetchTasksByUserId: async (accessToken, userId) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    const response = await fetch(baseUrlTasks + `/fetch/task/${userId}`, {
      method: "GET",
      headers: headers,
    });
    return response;
  },

  createTask: async (body, accessToken) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    await fetch(baseUrlTasks + "/create", {
      method: "POST",
      headers: headers,
      body: JSON.stringify(body),
    });
  },

  updateTask: async (updatedTask, accessToken) => {
    fetch(baseUrlTasks + "/update/task", {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedTask),
    });
  },

  deleteTask: async (taskId, accessToken) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
    const response = await fetch(baseUrlTasks + `/delete/${taskId}`, {
      method: "DELETE",
      headers: headers,
    });
    return response;
  },
}; // fine 

export const authAPI = {
  
  authenticate: async (body) => {
    
    const authenticate = await fetch(baseUrlAuth + "/authenticate",{
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    }
  );
  
  const authenticateData = await authenticate.json();
  return authenticateData;
  
},

register: async (body) => {
  
  const userRegister = await fetch(baseUrlAuth + "/register", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  
  return await userRegister.json();
},

logout: async () => {
  await fetch(baseUrlAuth + "/logout", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
  });
}



}; // fine 