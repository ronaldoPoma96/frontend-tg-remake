import { createWebHashHistory } from "vue-router";
import { createRouter } from "vue-router";

import Users from "./views/Users.vue";
import Tasks from "./views/Tasks.vue";
import TasksByUser from "./views/TasksByUser.vue";
import TaskCreate from "./views/TaskCreate.vue";

import Login from "./views/Login.vue";
import SignUp from "./views/SignUp.vue";




const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: "/login", component: Login },
    { path: "/sign-up", component: SignUp },
    { path: "/users", component: Users },
    { path: "/users/TasksByUser", component: TasksByUser },
    { path: "/tasks", component: Tasks },
    { path: "/tasks/create", component: TaskCreate },
  ],
});


export default router;